import {ExerciseSettings} from "../objects/ExerciseSettings";

class MathOperations
{
    static checkSubstitution(subLeft, subRight) {
        return window.twf_game.compareWithoutSubstitutions(subLeft, subRight);
    };

    static findSubstitutionPlacesCoordinates(exp, subLeft, subRight) {
        const positions = window.twf_game.findSubstitutionPlacesCoordinatesInExpressionJSON(exp, subLeft, subRight, ExerciseSettings.scope);
        // making object with appropriate types from JSON res
        let substitutionPlaces = (JSON.parse(positions)).substitutionPlaces;
        substitutionPlaces.forEach((subPlaces) => {
            for (let place in subPlaces) {
                subPlaces[place] = parseInt(subPlaces[place]);
            }
        });
        return substitutionPlaces;
    };

    static applySubstitutionByPlaceCoordinates(exp, subLeft, subRight, parentStartPod, parentEndPos, startPos, endPos) {
        let res = window.twf_game.applyExpressionBySubstitutionPlaceCoordinates(exp, subLeft, subRight, parentStartPod, parentEndPos, startPos, endPos, ExerciseSettings.scope);
        return (res[0] === '(' && res[res.length - 1] === ')') ? res.slice(1, -1) : res;
    };

    static isRuleApplicable(exp, subLeft, subRight) {
        const sub = MathOperations.findSubstitutionPlacesCoordinates(exp, subLeft, subRight, ExerciseSettings.scope);
        return !(Array.isArray(sub) && sub.length === 0);
    }

    // static applySubstitutionByRule(expression, rule) {
    //     let res = this.findSubstitutionPlacesCoordinates(expression, rule.left, rule.right);
    //     let resJSON = JSON.parse(res);
    //     let positions = resJSON.substitutionPlaces;
    //     if (positions.length === 0) return null;
    //     if (positions.length === 1) {
    //         return this.applySubstitutionByPlaceCoordinates(
    //             expression, rule.left, rule.right,
    //             parseInt(positions[0].parentStartPosition),
    //             parseInt(positions[0].parentEndPosition),
    //             parseInt(positions[0].startPosition),
    //             parseInt(positions[0].endPosition));
    //     }
    //     else {
    //         return positions;
    //     }
    // }

}

export default MathOperations