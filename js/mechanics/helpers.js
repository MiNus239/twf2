import {gameHeight, gameWidth} from "../appearance/game.js";

function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
}

function centerSprite(sprite, anchor=true) {
    sprite.x = gameWidth / 2;
    sprite.y = gameHeight / 2;
    if (anchor) sprite.anchor.set(0.5);
}

function removeSprite(sprite) {
    sprite.parent.removeChild(sprite);
}

const checkPerfomance = func => {
    const startAt = performance.now();
    func();
    const endAt = performance.now();
    return endAt - startAt;
};

// remove parenthesis
const removeParenthesis = word => word.replace(/\(/g, '').replace(/\)/g, '');

export const addRuleToCollectionIfNotExists = (rule, collection) => {
    if (collection.indexOf(rule) === -1) collection.push(rule);
};

export const clearContainer = container => {
    while (container.children[0]) container.removeChild(container.children[0]);
};

export { shuffleArray, centerSprite, removeSprite, checkPerfomance, removeParenthesis }