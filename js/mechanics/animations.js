import MathOperations from "./MathOperations.js";
import { MutationBlock } from "../objects/Block.js";
import {game} from "../appearance/game.js";
import {gameHeight, gameWidth, tileDimensions} from "../appearance/game.js";
import {Mechanism} from "../objects/static/Mechanism.js";
import {Steps} from "../objects/Steps.js";
import {showCross} from "../objects/static/Cross.js";
import {Goal} from "../index.js";
import {Hearts} from "../objects/Hearts.js";
import {addAlert} from "../appearance/alertScreen.js";
import {removeParenthesis} from "./helpers.js";
import {createTextSprite} from "../objects/text.js";
import {ExerciseSettings} from "../objects/ExerciseSettings";
import {RuleBlock} from "../objects/Block";

const textStyle = new PIXI.TextStyle({
    fill: 'red',
});

function createText(text, x, y, red=false) {
    let sprite = createTextSprite(text, textStyle);
    sprite.x = x;
    sprite.y = y;
    sprite.style = {
        fontFamily: 'Arial',
        fontSize: gameWidth / 60,
        fontStyle: 'italic',
        fontWeight: 'bold',
    };
    if (red) {
        sprite.style.fill = 'red';
        sprite.interactive = true;
        sprite.buttonMode = true;
    }
    return sprite;
}

function fall(block) {
    const falling = setInterval(() => {
        if (block.y >= MutationBlock.y) {
            block.parent.removeChild(block);
            clearInterval(falling);
        }
        block.y += tileDimensions / 5;
    }, 1);
}

function makeSingleSubstitution(block, subPlace) {
    const mutation = MathOperations.applySubstitutionByPlaceCoordinates(
        MutationBlock.topExpression,
        block.rule.left,
        block.rule.right,
        subPlace.parentStartPosition,
        subPlace.parentEndPosition,
        subPlace.startPosition,
        subPlace.endPosition
    );
    // place block under the mechanism
    block.parent.removeChild(block);
    game.stage.addChild(block);
    block.y = gameHeight / 2;
    block.x = gameWidth / 2;
    // fall and new Mutation block
    fall(block);
    new MutationBlock(mutation);
    // manage steps
    Steps.takeStep();
    if (removeParenthesis(MutationBlock.topExpression) === removeParenthesis(Goal.expression)) {
        //win
        addAlert(true);
    }
    if (Steps.isLimitReached()) {
        //loss
        Hearts.deleteHeart();
        Goal.moveUp();
        if (Hearts.heartsLeft() === 0) addAlert();
    }
}

function errorSubstitution(block) {
    block.x = block.startX;
    block.y = block.startY;

    showCross();
}

function releaseRuleBlock(block, rules, steps) {
    const choiceTextSprites = [];

    console.log(MutationBlock.topExpression);
    const subPlaces = MathOperations.findSubstitutionPlacesCoordinates(
        MutationBlock.topExpression,
        block.rule.left,
        block.rule.right
    );

    if (subPlaces.length === 0) {
        errorSubstitution(block);
    }

    else if (subPlaces.length === 1) {
        makeSingleSubstitution(block, subPlaces[0], steps)
    }

    else if (subPlaces.length > 1) {
        // mechanism.sprite.onLoop = () => mechanism.sprite.stop();
        // API bug for -1, -1 positions
        if (subPlaces[0].startPosition === -1 || subPlaces[1].startPosition === -1) {
            errorSubstitution(block);
        }
        // put block under the mechanism until choice is made
        Mechanism.await();
        block.parent.removeChild(block);
        game.stage.addChild(block);
        block.y = gameHeight / 2;
        block.x = gameWidth / 2;

        const x = gameWidth / 2;
        const y = gameHeight / 2;
        let bgText = createText(
            MutationBlock.topExpression,
            x, y
        );
        bgText.x -= bgText.width / 2;
        bgText.y -= bgText.height / 2;
        bgText.y += tileDimensions * 2;

        game.stage.addChild(bgText);
        choiceTextSprites.push(bgText);

        subPlaces.forEach((places) => {
            let subSegmentSprite = null;

            if (places.startPosition === 0) {
                const subSegmentText = MutationBlock.topExpression.slice(0, places.endPosition + 1);
                subSegmentSprite = createText(subSegmentText, x, y, true);
                subSegmentSprite.x -= bgText.width / 2;
                subSegmentSprite.y -= bgText.height / 2;
                subSegmentSprite.y += tileDimensions * 2;
                game.stage.addChild(subSegmentSprite);
            }

            if (places.startPosition > 0) {
                const skipText = MutationBlock.topExpression.slice(0, places.startPosition);
                const skipSprite = createText(skipText, x, y);

                const subSegmentText = MutationBlock.topExpression.slice(places.startPosition, places.endPosition + 1);
                subSegmentSprite = createText(
                    subSegmentText,
                    x + skipSprite.width,
                    y,
                    true
                );
                subSegmentSprite.x -= bgText.width / 2;
                subSegmentSprite.y -= bgText.height / 2;
                subSegmentSprite.y += tileDimensions * 2;
                game.stage.addChild(subSegmentSprite);
            }
            choiceTextSprites.push(subSegmentSprite);
            subSegmentSprite.on('click', () => {
                makeSingleSubstitution(block, places, steps);
                Mechanism.continue();
                choiceTextSprites.forEach(text => game.stage.removeChild(text));
            });
        });
    }
}

class Dragging
{
    static onDragStart(block, event) {
        if (RuleBlock.isDraggingPossible === true) {
            block.startX = block.x;
            block.startY = block.y;
            block.data = event.data;
            block.dragging = true;
            block.alpha = 0.8;
            block.zIndex = 2;
        }
    }

    static onDragMove(block) {
        if (block.dragging === true && RuleBlock.isDraggingPossible === true) {
            const newPosition = block.data.getLocalPosition(block.parent);
            block.x = newPosition.x;
            block.y = newPosition.y;
        }
    }

    static onDragEnd(block, rules, steps) {
        if (RuleBlock.isDraggingPossible === true) {
            block.alpha = 1;
            block.zIndex = 1;
            block.dragging = false;
            if (
                (block.y > gameHeight / 2 - tileDimensions * 4)
                // &&
                // (block.x > tileDimensions * 8 && block.x < gameWidth / 2 + tileDimensions * 8)
                // TODO fix horizontal position
            ) {
                releaseRuleBlock(block, rules, steps);
            } else {
                block.x = block.startX;
                block.y = block.startY;
                block.rotation = 0;
            }
        }
    }
}

export { Dragging }