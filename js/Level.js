import {addRuleToCollectionIfNotExists} from "./mechanics/helpers.js";

export class Level
{
    static levels = [];
    static attendantRules = [];

    static currentIndex = 0;
    static getIndexOfLast = () => Level.levels.length - 1;
    static next = () => Level.currentIndex++;

    constructor(
        steps,
        scope,
        startingExpression,
        goalExpression,
        hearts,
        time,
        optimalRules,
        attendantRules
    )
    {
        Object.assign(this, {
            steps,
            scope,
            startingExpression,
            goalExpression,
            hearts,
            time,
            optimalRules,
            attendantRules
        });

        Level.levels.push(this);

         attendantRules.forEach(rule => {
             [rule, {left: rule.right, right: rule.left}].forEach(rule => {
                 addRuleToCollectionIfNotExists(rule, this.attendantRules)
             });
             // addRuleToCollectionIfNotExists(rule, Level.attendantRules);
         });
    }
}

// ADDING LEVELS
const level0 = new Level(
    2,
    "setTheory",
    "!A&!B&!C",
    "!A&(!B\\C)",
    3,
    60,
    [
        {
            left: "A&B&C",
            right: "A&(B&C)"
        },
        {
            left: "A&!B",
            right: "A\\B"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        {
            left: "A&B&C",
            right: "A&(B&C)"
        },
        {
            left: "!A&B",
            right: "B\\A"
        }
    ]
);

const level1 = new Level(
    2,
    "",
    "(sin(x))^2-(cos(x))^2",
    "-cos(-2*x)",
    3,
    60,
    [
        {
            left: "(sin(x))^2-(cos(x))^2",
            right: "-cos(2*x)"
        },
        {
            left: "cos(x)",
            right: "cos(-x)"
        }
    ],
    [
        {
            left: "(sin(x))^2",
            right: "1-(cos(x))^2"
        },
        {
            left: "(cos(x))^2",
            right: "1-(sin(x))^2"
        },
        {
            left: "cos(-x)",
            right: "cos(x)"
        },
        {
            left: "cos(2*x)",
            right: "(cos(x))^2-(sin(x))^2"
        },
        {
            left: "cos(2*x)",
            right: "2*(cos(x))^2-1"
        },
        {
            left: "cos(2*x)",
            right: "1-2*(sin(x))^2"
        },
        {
            left: "(x-y)-y",
            right: "x-2*y"
        },
        {
            left: "-cos(2*x)",
            right: "1-2*(cos(x))^2"
        },
        {
            left: "x+1-1",
            right: "x"
        }
    ]
);

 const level2 = new Level(
     3,
     "",
     "ctg(-x)*tg(-x)-(sin(-x))^2",
     "cos(x)^2",
     4,
     70,
     [
         {
             left: "ctg(x)*tg(x)",
             right: "1"
         },
         {
             left: "1-(sin(x))^2",
             right: "(cos(x))^2"
         },
         {
             left: "cos(-x)",
             right: "cos(x)"
         }
     ],
     [
         {
             left: "ctg(x)",
             right: "cos(x)/sin(x)"
         },
         {
             left: "tg(x)",
             right: "sin(x)/cos(x)"
         },
         {
             left: "1-(1-x)",
             right: "x"
         },
         {
             left: "(x/y)*(y/x)",
            right: "1"
         }
    ]
 );

// const level3 = new Level(
//     3,
//     "",
//     "cos(2*x)",
//     "2*cos(x)^2-1",
//     4,
//     70,
//     [
//         {
//             left: "cos(2*x)",
//             right: "cos(x)^2-sin(x)^2"
//         },
//         {
//             left: "sin(x)^2",
//             right: "1-cos(x)^2"
//         },
//         {
//             left: "x - (y - x)",
//             right: "2*x - y"
//         }
//     ],
//     [
//         // TODO ADD
//         {
//             left: "cos(2*x)",
//             right: "cos(x)^2-sin(x)^2"
//         }
//     ]
// );
//
// const level4 = new Level(
//     4,
//     "",
//     "1/(cos(x))^2",
//     "(tg(x))^2+1",
//     4,
//     90,
//     [
//         {
//             left: "1",
//             right: "(sin(x))^2+(cos(x))^2"
//         },
//         {
//             left: "(x + y) / z",
//             right: "x/z + y/z"
//         },
//         {
//             left: "(sin(x))^2/(cos(x))^2",
//             right: "(tg(x))^2"
//         },
//         {
//             left: "x/x",
//             right: "1"
//         },
//     ],
//     [
//         // TODO ADD
//         {
//             left: "cos(2*x)",
//             right: "cos(x)^2-sin(x)^2"
//         }
//     ]
// );
//
// const level5 = new Level(
//     5,
//     "",
//     "cos(x)/(1-sin(x)) + cos(x)/(1+sin(x))",
//     "2/cos(x)",
//     5,
//     100,
//     [
//         {
//             left: "x/y + x/z",
//             right: "x * (1/y + 1/z)"
//         },
//         {
//             left: "1/x + 1/y",
//             right: "2/x*y"
//         },
//         {
//             left: "(1-x) * (1+x)",
//             right: "1 - x^2"
//         },
//         {
//             left: "1-(sin(x))^2",
//             right: "(cos(x))^2"
//         },
//         {
//             left: "x*(y/x^2)",
//             right: "y/x"
//         }
//     ],
//     [
//         // 1st rule
//         {
//             left: "cos(x)",
//             right: "cos(-x)"
//         },
//         {
//             left: "cos(x)",
//             right: "1-2*(sin(x/2))^2"
//         },
//         {
//             left: "cos(x)",
//             right: "2*(cos(x/2))^2-1"
//         },
//         {
//             left: "cos(x)",
//             right: "(cos(x/2))^2-(sin(x/2))^2"
//         },
//     ]
// );

// const attendantRules = [
//     {
//         left: "sin(x)",
//         right: "2*sin(x/2)*cos(x/2)"
//     },
//     {
//         left: "sin(-x)",
//         right: "-sin(x)"
//     }
// ];

const attendantRules = [
];

//attendantRules.forEach(rule => addRuleToCollectionIfNotExists(rule, Level.attendantRules));