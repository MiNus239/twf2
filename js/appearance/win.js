import {makeObjectStageSwitcher} from "./stageManagement.js";
import {menu} from "./menu.js";
import { gameHeight, gameWidth, tileDimensions} from "./game.js";
import {createText} from "../objects/text.js";
import {createTextSprite} from "../objects/text.js";

const winStage = new PIXI.Application({
    backgroundColor: 0x22303e,
    autoDensity: true,
    resizeTo: window
});

const textStyle = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: tileDimensions * 2,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradient
    // stroke: '#4a1850',
    stroke: '#653444',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
});

const congrats = createTextSprite(`Congratulations! You won!`, textStyle);
congrats.x = gameWidth / 2;
congrats.y = gameHeight / 2;
congrats.anchor.set(0.5);
winStage.stage.addChild(congrats);

const reloadText = createTextSprite(`Reload this page to try again!`, createText(gameWidth / 25));
reloadText.x = gameWidth / 2;
reloadText.y = congrats.y + congrats.height + tileDimensions;
reloadText.anchor.set(0.5);
winStage.stage.addChild(reloadText);

export {winStage}