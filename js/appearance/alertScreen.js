import {game, gameHeight, gameWidth, scale, tileDimensions} from "./game.js";
import {renderExercise} from "../index.js";
import {Level} from "../Level.js";
import {makeBordersAroundSprite} from "./renderBg.js";
import {Timer} from "../objects/Timer.js";
import {generateComment} from "./Comments.js";
import {mainTextStyle} from "../objects/text.js";
import {createTextSprite} from "../objects/text.js";
import {RuleBlock} from "../objects/Block";
import {Chain} from "../objects/static/Chain";
import {RuleBlockContainer} from "../index";
import {Mechanism} from "../objects/static/Mechanism";

export class IsGameEnded {
    static isGameEnded = false;
}

export function addAlert (win=false) {
    RuleBlock.allRuleBlocksArr.forEach(ruleBlock => {
        ruleBlock.stop();
        RuleBlock.isDraggingPossible = false;
    });
    RuleBlockContainer.children.forEach(ruleBlock => ruleBlock.stop());
    Timer.stop();
    Mechanism.await();
    Chain.stop();
    IsGameEnded.isGameEnded = true;
    setTimeout(() =>{
        RuleBlock.allRuleBlocksArr.forEach(ruleBlock => ruleBlock.stop());
        }, 20);

    const loader = new PIXI.Loader();
    loader.add("./assets/alertScreen.png");
    loader.onComplete.add(() => {
        const alert = new PIXI.Container();
        const bgTexture = loader.resources["./assets/alertScreen.png"].texture;
        const bg = new PIXI.Sprite(bgTexture);
        alert.addChild(bg);

        const numOfBorderTilesHeight = Math.round(alert.height / tileDimensions) + 1;
        const numOfBorderTilesWidth = Math.round(alert.width / tileDimensions);

        makeBordersAroundSprite(alert, numOfBorderTilesWidth, numOfBorderTilesHeight, "front");

        alert.scale.set(scale * 1.5);
        alert.x = gameWidth / 2 - alert.width / 2;
        alert.y = gameHeight / 2 - alert.height / 2;
        game.stage.addChild(alert);

        const transparentBg = setInterval(() => game.stage.children.forEach(child => {
            if (child !== alert) child.alpha = 0.5;
        }), 10);

        const alertTextStyle = new PIXI.TextStyle(mainTextStyle);
        alertTextStyle.fontSize *= 1.5;

        const msgText = win ? `You Win!` : `You lost!`;
        const msgSprite = createTextSprite(msgText, alertTextStyle);
        // msgSprite.resolution = 2
        alert.addChild(msgSprite);
        msgSprite.anchor.set(0.5);
        msgSprite.x += numOfBorderTilesWidth / 2 * tileDimensions;
        msgSprite.y += tileDimensions * 2;

        const commentText = generateComment(win, Level.currentIndex);
        const commentTextStyle = new PIXI.TextStyle(mainTextStyle);
        commentTextStyle.wordWrap = true;
        commentTextStyle.wordWrapWidth = (numOfBorderTilesWidth - 4) * tileDimensions;
        commentTextStyle.fontSize *= 1.5;
        commentTextStyle.align = 'center'
        const comment = createTextSprite(commentText, commentTextStyle);
        comment.resolution = 2;
        comment.y += tileDimensions * 4;
        comment.x = numOfBorderTilesWidth / 2 * tileDimensions;
        comment.anchor.x = 0.5;
        alert.addChild(comment);

        const repeat = createTextSprite("Try again", alertTextStyle);
        repeat.x += tileDimensions;
        repeat.y += numOfBorderTilesHeight * tileDimensions - tileDimensions * 4;
        alert.addChild(repeat);

        const next = createTextSprite("Next", alertTextStyle);
        next.x += numOfBorderTilesWidth * tileDimensions - tileDimensions * 2 - next.width;
        next.y += numOfBorderTilesHeight * tileDimensions - tileDimensions * 4;

        if (win && Level.currentIndex !== Level.getIndexOfLast()) alert.addChild(next);

        [repeat, next].forEach(button => {
            button.interactive = true;
            button.buttonMode = true;
        });

        repeat.on("pointerdown", () => {
            RuleBlock.isDraggingPossible = true;
            //IsGameEnded.isGameEnded = false;
            clearInterval(transparentBg);
            renderExercise();
        });

        next.on("pointerdown", () => {
            RuleBlock.isDraggingPossible = true;
            //IsGameEnded.isGameEnded = false;
            clearInterval(transparentBg);
            Level.next();
            renderExercise();
        });

    });
    loader.load();
}
