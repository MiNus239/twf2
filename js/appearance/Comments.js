const winQuotes = [
    // 1st level
    [
        "You rarely win, but sometimes you do"
    ],
    // 2nd level
    [
        "It's easy to win. Anybody can win"
    ],
    // 3d level
    [
        "Winning isn’t everything; it’s just the ONLY thing"
    ],
    // 4th level
    [
        "If You Push Yourself For More, You'll Find More"
    ],
    // 5th level
    [
        "You are a math genius! Incas would be proud of you!"
    ],
];

const loseQuotes = [
    // 1st level
    [
        "If you learn from a loss you have not lost"
    ],
    // 2nd level
    [
        "I would prefer even to lose with honor than to win by cheating",
    ],
    // 3d level
    [
        "The trouble with being a good sport is that you have to lose to prove it"
    ],
    // 4th level
    [
        "Winners have to absorb losses"
    ],
    // 5th level
    [
        "Always imitate the behavior of the winners when you lose"
    ],
];

export const generateComment = (win=false, levelNum) => {
    if (win) return winQuotes[levelNum][0];
    else return loseQuotes[levelNum][0];
};