import {game, ruleBlocksWrapper} from "../appearance/game.js";
import {Dragging} from "../mechanics/animations.js";
import {gameWidth, gameHeight, tileDimensions, scale} from "../appearance/game.js";
import {numOfBorderTilesWidth} from "../appearance/game.js";
import {generateArrayOfRandomAssets} from "../appearance/renderGamingBlocks.js";
import {centerSprite} from "../mechanics/helpers.js";
import {createChainForBlock} from "./static/Chain.js";
import {ruleBlocksContainerY} from "../appearance/constantPositions.js";
import {removeSprite} from "../mechanics/helpers.js";
import {removeParenthesis} from "../mechanics/helpers.js";
import {Goal} from "../index.js";
import {movingSpeed} from "../appearance/game.js";
import {Level} from "../Level.js";
import MathOperations from "../mechanics/MathOperations.js";
import {shuffleArray} from "../mechanics/helpers.js";
import {clearContainer} from "../mechanics/helpers.js";
import { mainTextStyle} from "./text.js";
import {createTextSprite} from "./text.js";
import {ExerciseSettings} from "./ExerciseSettings";

const textStyle = new PIXI.TextStyle(mainTextStyle);

function makeArrayOfAppropriateRules(optimalRules, attendantRules, expression) {
    return optimalRules.concat(attendantRules).filter(rule => MathOperations.isRuleApplicable(expression, rule.left, rule.right));
}

function createGamingBlock(container, expressionSprite, numOfBlocks, blockStyle, scaleCoefficient, alpha=null) {
    let urls = generateArrayOfRandomAssets(numOfBlocks, blockStyle);
    for (let i = 0; i < urls.length; i++) {
        const sprite = new PIXI.Sprite.from(urls[i]);
        sprite.scale.set(scaleCoefficient * scale);
        sprite.x = i * scaleCoefficient * tileDimensions;
        sprite.y = 0;
        if (alpha) sprite.alpha = alpha;
        container.addChild(sprite);
    }

    container.addChild(expressionSprite);
    expressionSprite.anchor.set(0.5);
    expressionSprite.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    expressionSprite.y = tileDimensions;

    container.pivot.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    container.pivot.y = tileDimensions * scaleCoefficient / 2;

    return container;
}

class RuleBlock
{
    static lowered = false;
    static allRuleBlocksArr = [];
    static isDraggingPossible = true;
    static resetRuleBlock() {
        RuleBlock.lowered = false;
    }
    constructor(rule) {
        RuleBlock.lowered = !RuleBlock.lowered;

        this.sprite = new PIXI.Container();
        this.sprite.sortableChildren = true;

        const exp = createTextSprite(`${rule.left} = ${rule.right}`, textStyle);
        const numOfBlocks = Math.ceil(exp.width / (2 * tileDimensions)) >= 3
            ? Math.ceil(exp.width / (2 * tileDimensions))
            : 3;
        this.block = createGamingBlock(
            new PIXI.Container(),
            exp,
            numOfBlocks,
            "front",
            2
        );
        this.block.rule = rule;
        this.block.interactive = true;
        this.block.buttonMode = true;
        this.block.zIndex = 2;
        this.block
            .on('pointerdown', event => Dragging.onDragStart(this.block, event))
            .on('pointerup', () => Dragging.onDragEnd(this.block, rule))
            .on('pointerupoutside', () => Dragging.onDragEnd(this.block, rule))
            .on('pointermove', () => Dragging.onDragMove(this.block));

        this.chain = createChainForBlock(RuleBlock.lowered);
        this.block.y += this.chain.height;

        this.sprite.addChild(this.block);
        this.sprite.addChild(this.chain);

        // make it move
        this.intervalFunction = () => {
            this.sprite.x += movingSpeed;
            if (this.sprite.x > gameWidth + numOfBlocks * tileDimensions * 2) {
                clearInterval(this.sprite.interval);
                removeSprite(this.sprite);
            }
        };

        this.sprite.stop = () => clearInterval(this.sprite.interval);

        this.sprite.continue = () => this.sprite.interval = setInterval(this.intervalFunction, 10);

        this.sprite.interval = this.sprite.continue();

        RuleBlock.allRuleBlocksArr.push(this.sprite);
    }
}

// class RuleBlockContainerClass
// {
//     constructor() {
//         this.sprite = new PIXI.Container();
//         this.sprite.y = tileDimensions * 3;
//
//         this.sprite.makeLoop = (lastChildWidth=null) => {
//             this.sprite = new PIXI.Container();
//             this.sprite.y = tileDimensions * 3;
//             game.stage.addChild(this.sprite)
//
//             const currentLevel = Level.levels[Level.currentIndex];
//             const currentMutation = MutationBlock.topExpression;
//
//             const arrayOfAppropriateRules = makeArrayOfAppropriateRules(currentLevel.optimalRules, Level.attendantRules, currentMutation);
//             // fix if array empty to prevent game crash
//             if (arrayOfAppropriateRules.length <= 1) {
//                 while (arrayOfAppropriateRules.length !== 5) {
//                     arrayOfAppropriateRules.push(Level.attendantRules[Math.floor(Math.random() * Level.attendantRules.length)]);
//                 }
//             }
//             shuffleArray(arrayOfAppropriateRules);
//
//             let currentArrIndex = 0;
//
//             const firstBlock = new RuleBlock(arrayOfAppropriateRules[0]).sprite;
//             // fix block overlay while moving new array
//             if (lastChildWidth) firstBlock.x -= lastChildWidth;
//             firstBlock.children[0].y = RuleBlock.lowered
//                 ? this.sprite.y + tileDimensions * 5
//                 : this.sprite.y + tileDimensions;
//             this.sprite.addChild(firstBlock);
//             this.sprite.x -= firstBlock.width / 2;
//
//             const interval = setInterval(() => {
//                 const lastChild = this.sprite.children[this.sprite.children.length - 1];
//
//                 if (MutationBlock.topExpression !== currentMutation) {
//                     clearInterval(interval);
//                     this.sprite.makeLoop(currentLevel, lastChild.width);
//                 }
//                 else {
//                     if (this.sprite.children.length === 0) {
//                         const block = new RuleBlock(arrayOfAppropriateRules[0]).sprite;
//                         this.sprite.addChild(block);
//                         currentArrIndex++;
//                         if (currentArrIndex >= arrayOfAppropriateRules.length) currentArrIndex = 0;
//                     }
//                     else {
//                         if (lastChild.x >= lastChild.width / 2) {
//                             const newBlock = new RuleBlock(arrayOfAppropriateRules[currentArrIndex]).sprite;
//                             newBlock.x -= newBlock.width / 2;
//                             newBlock.x += tileDimensions * 2;
//                             this.sprite.addChild(newBlock);
//                             currentArrIndex++;
//                             if (currentArrIndex >= arrayOfAppropriateRules.length) currentArrIndex = 0;
//                         }
//                     }
//                 }
//             }, 1);
//         };
//     }
// }

class MutationBlock
{
    static y = gameHeight - tileDimensions * 2;
    static x = gameWidth / 2 + tileDimensions * 3;
    static numOfBlocks = numOfBorderTilesWidth / 2;
    static topExpression = "";
    static allBlocksArr = [];

    static getAmountOfBlocks = () => MutationBlock.allBlocksArr.length;

    static lowerBlocksDown = () => {
        const firstBlock = MutationBlock.allBlocksArr.shift();
        removeSprite(firstBlock);
        MutationBlock.allBlocksArr.forEach(block => block.y += tileDimensions * 2);
        MutationBlock.y += tileDimensions * 2;
    };

    static resetMutationBlock = () => {
        MutationBlock.y = gameHeight - tileDimensions * 2;
        MutationBlock.x = gameWidth / 2 + tileDimensions * 3;
        MutationBlock.numOfBlocks = numOfBorderTilesWidth / 2;
        MutationBlock.topExpression = "";
        MutationBlock.allBlocksArr = [];
    };

    constructor(expression) {
        MutationBlock.topExpression = expression;
        const exp = createTextSprite(expression, textStyle);
        const scaleCoefficient = 2;
        const numOfBlocks = MutationBlock.numOfBlocks;


        this.sprite = new PIXI.Container();
        this.sprite.urls = generateArrayOfRandomAssets(numOfBlocks, "front");
        this.sprite.numOfBlocks = numOfBlocks;

        // fill Container with small blocks
        for (let i = 0; i < this.sprite.urls.length - 3; i++) {
            const sprite = new PIXI.Sprite.from(this.sprite.urls[i]);
            sprite.scale.set(scaleCoefficient * scale);
            sprite.x = i * scaleCoefficient * tileDimensions;
            sprite.y = 0;
            this.sprite.addChild(sprite);
        }

        this.sprite.pivot.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
        this.sprite.pivot.y = tileDimensions;

        this.sprite.x = MutationBlock.x;
        this.sprite.y = MutationBlock.y;

        this.sprite.addChild(exp);
        exp.anchor.set(0.5);
        exp.x = numOfBlocks * tileDimensions - 3 * tileDimensions;
        exp.y = tileDimensions;

        game.stage.addChild(this.sprite);
        MutationBlock.y -= tileDimensions * 2;
        MutationBlock.numOfBlocks -= 2;
        MutationBlock.allBlocksArr.push(this.sprite);

        // lower down pyramid
        if (
            MutationBlock.getAmountOfBlocks() === 5
            && removeParenthesis(MutationBlock.topExpression) !== removeParenthesis(Goal.expression)
        ) MutationBlock.lowerBlocksDown();
    }
}

class GoalClass
{
    static y;
    static resetGoal = () => GoalClass.y = 0;

    constructor(expression, steps) {
        this.expression = expression;

        this.numOfBlocks = MutationBlock.numOfBlocks - steps * 2 - 1;
        this.goalBlock = createGamingBlock(
            new PIXI.Container(),
            createTextSprite(expression, textStyle),
            this.numOfBlocks,
            "dark",
            2,
            0.7
        );

        this.arrow = new PIXI.Sprite.from('./assets/arrow.png');
        this.arrow.scale.set(2 * scale);
        this.arrow.anchor.set(0.5);
        this.arrow.x -= this.goalBlock.width / 2 + tileDimensions * 2;

        let fade = false;
        setInterval(() => {
            fade ? this.arrow.alpha = 0.4 : this.arrow.alpha = 0.7;
            fade = !fade;
        }, 500);

        const initialHeight = steps >= 4
            ? gameHeight - tileDimensions * 10
            : gameHeight - (tileDimensions * 2) * (steps + 1);

        this.mainContainer = new PIXI.Container();
        this.mainContainer.x = gameWidth / 2;
        this.mainContainer.y = initialHeight;
        GoalClass.y = initialHeight;
        this.mainContainer.addChild(this.goalBlock);
        this.mainContainer.addChild(this.arrow);
        game.stage.addChild(this.mainContainer);

        this.moveUp = () => {
            this.mainContainer.removeChild(this.goalBlock);
            if (this.numOfBlocks > 4) {
                this.numOfBlocks -= 2;
                this.arrow.x += tileDimensions * 2;
            }
            this.goalBlock = createGamingBlock(
                new PIXI.Container(),
                createTextSprite(expression, textStyle),
                this.numOfBlocks,
                "dark",
                2,
                0.7
            );
            this.mainContainer.addChild(this.goalBlock);
            if (this.mainContainer.y !== gameHeight - tileDimensions * 10) {
                this.mainContainer.y -= tileDimensions * 2;
                GoalClass.y -= tileDimensions * 2;
            }
        };
    }
}

export { RuleBlock, MutationBlock, GoalClass }