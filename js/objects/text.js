import { tileDimensions } from "../appearance/game.js";
import {scale} from "../appearance/game.js";

export const mainTextStyle = {
    fontFamily: 'sans serif',
    fontSize: tileDimensions * scale,
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradient
    stroke: '#653444',
    strokeThickness: 4,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 4,
    breakWords: true,
};

export const createTextSprite = (msg, style) => {
    const textSprite = new PIXI.Text(msg, style);
    textSprite.resolution = 2;
    return textSprite;
};