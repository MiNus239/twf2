import {game, gameWidth, numOfBorderTilesWidth, tileDimensions} from "../../appearance/game.js";
import {gameHeight} from "../../appearance/game.js";

class PlaceHolder {
    constructor() {
        this.sprite = new PIXI.Graphics();

        this.sprite.lineStyle(2, 0xB0664B, 1);
        this.sprite.drawRoundedRect(
            gameWidth / 2,
            gameHeight / 2,
            tileDimensions * numOfBorderTilesWidth / 3,
            tileDimensions * 2,
            16
        );
        this.sprite.pivot.x = this.sprite.width / 2;
        this.sprite.pivot.y = this.sprite.height / 2;

        // blinking
        let fade = false;
        setInterval(() => {
            this.fade ? this.sprite.alpha = 0.4 : this.sprite.alpha = 0.7;
            this.fade = !this.fade;
        }, 500);

        game.stage.addChild(this.sprite);
    }
}

export {PlaceHolder}

