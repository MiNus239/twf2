import { game, scale, tileDimensions } from "../../appearance/game.js";
import { renderExercise } from "../../index.js"
import {IsGameEnded} from "../../appearance/alertScreen";

export function addRetryButtonToStage() {
    const retryButton = new PIXI.Sprite.from('./assets/retry.png');
    retryButton.anchor.set(0.5);
    retryButton.scale.set(scale * 0.5);
    retryButton.x = tileDimensions * 3.5;
    retryButton.y = tileDimensions * 2;
    game.stage.addChild(retryButton);

    retryButton.interactive = true;
    retryButton.buttonMode = true;

    retryButton.on("pointerup", () => {
        IsGameEnded.isGameEnded = true;
        //setTimeout(() => IsGameEnded.isGameEnded = false, 100);
        renderExercise();
    });
}