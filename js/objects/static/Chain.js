import { game, tileDimensions, scale } from "../../appearance/game.js";
import {ruleBlocksContainerY } from "../../appearance/constantPositions.js";
import {movingSpeed} from "../../appearance/game.js";
import {gameWidth} from "../../appearance/game.js";
import {removeSprite} from "../../mechanics/helpers.js";

class ChainClass {
    constructor() {
        this.sprite = new PIXI.Container();
        const numOfChainPiecesWidth = Math.floor(game.view.width / tileDimensions);
        for (let i = 0; i < numOfChainPiecesWidth * 2; i++) {
            const piece = new PIXI.Sprite.from('./assets/tiny-rope.png');
            piece.scale.set(scale * 4);
            piece.x = i * tileDimensions;
            piece.rotation = Math.PI / 2;
            this.sprite.addChild(piece);
        }
        this.sprite.y = ruleBlocksContainerY;
        this.sprite.x -= this.sprite.width / 2;

        this.sprite.addToStage = () => {
            game.stage.addChild(this.sprite);
            this.sprite.continue();
        };

        this.intervalFunction = () => this.sprite.x >= 0 ? this.sprite.x = -this.sprite.width / 2 : this.sprite.x += movingSpeed;

        this.sprite.stop = () => clearInterval(this.sprite.interval);

        this.sprite.continue = () => this.sprite.interval = setInterval(this.intervalFunction, 10);
    }
}

// export function addMovingChainToStage() {
//     const chain = new Chain().sprite;
//     game.stage.addChild(chain);
//     setInterval(() => chain.x >= 0 ? chain.x = -chain.width / 2 : chain.x += movingSpeed, 10);
// }

export function createChainForBlock(lowered) {
    const chain = new PIXI.Container();
    const length = lowered ? 8 : 4;
    for (let i = 0; i < length; i++) {
        const y = tileDimensions / 1.2;
        const piece = new PIXI.Sprite.from('./assets/tiny-rope.png');
        piece.scale.set(scale * 4);
        piece.y = y + tileDimensions * i;
        piece.anchor.set(0.5);
        chain.addChild(piece);
    }
    return chain;
}

export const Chain = new ChainClass().sprite;