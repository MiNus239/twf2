import {game, scale, gameHeight, gameWidth} from "../../appearance/game.js";
import {centerSprite} from "../../mechanics/helpers.js";

class MechanismClass
{
    constructor() {
        const frames = [];
        // sprite has 12 frames
        for (let i = 0; i < 11; i++) frames.push(PIXI.Texture.from(`./assets/mechanism/tile_${i}.png`));

        this.sprite = new PIXI.AnimatedSprite(frames);
        centerSprite(this.sprite);
        this.sprite.animationSpeed = 0.3;
        this.sprite.rotation = Math.PI / 2;
        this.sprite.scale.set(3 * scale);
        this.sprite.play();

        this.sprite.addToStage = () => {
            game.stage.addChild(this.sprite);
        };

        this.sprite.await = () => {
            this.sprite.onLoop = () => this.sprite.stop();
        };

        this.sprite.continue = () => {
            this.sprite.onLoop = null;
            this.sprite.play();
        };
    }
}

export const Mechanism = new MechanismClass().sprite;