import { tileDimensions, scale, game } from "../../appearance/game.js";
import {RuleBlockContainer} from "../../index.js";
import {Timer} from "../Timer.js";
import {centerSprite} from "../../mechanics/helpers.js";
import {MutationBlock} from "../Block.js";
import {gameWidth} from "../../appearance/game.js";
import {RuleBlock} from "../Block.js";
import {Goal} from "../../index.js";
import {GoalClass} from "../Block.js";
import {removeSprite} from "../../mechanics/helpers.js";
import {ruleBlocksContainerY} from "../../appearance/constantPositions.js";
import {gameHeight} from "../../appearance/game.js";
import {ruleBlocksWrapper} from "../../appearance/game.js";
import {Mechanism} from "./Mechanism.js";
import {Chain} from "../static/Chain.js";
import {createTextSprite} from "../text.js";

function makeTip(arrowX, arrowY, arrowRotation, msg, callBackFunction) {
    const arrow = new PIXI.Sprite.from('./assets/info-arrow.png');
    arrow.scale.set(scale * 2);
    arrow.anchor.set(0.5);
    arrow.x = arrowX;
    arrow.y = arrowY;
    arrow.rotation = arrowRotation;
    let pos = true;
    arrow.interval = setInterval(() => {
        if (arrowRotation === 0 || arrowRotation === -Math.PI) {
            if (arrow.x > arrowX + tileDimensions / 2 || arrow.x < arrowX - tileDimensions / 2) pos = !pos;
            if (pos) arrow.x += 0.1;
            else arrow.x -= 0.1;
        }
        else {
            if (arrow.y > arrowY + tileDimensions / 2 || arrow.y < arrowY - tileDimensions / 2) pos = !pos;
            if (pos) arrow.y += 0.1;
            else arrow.y -= 0.1;
        }
    }, 1);
    game.stage.addChild(arrow);

    const text = createTextSprite(msg, {
        fontSize: tileDimensions,
        fill: 'red',
        wordWrapWidth: gameWidth - 4 * tileDimensions,
        wordWrap: true,
        align: "center"
    });
    text.anchor.set(0.5);
    text.x = gameWidth / 2;
    text.y = gameHeight / 2;
    game.stage.addChild(text);

    const nextBtn = new PIXI.Sprite.from('./assets/ok.png');
    nextBtn.scale.set(scale * 1.5);
    nextBtn.anchor.set(0.5);
    nextBtn.x = gameWidth / 2;
    nextBtn.y = gameHeight / 2 + text.height + tileDimensions;
    game.stage.addChild(nextBtn);

    nextBtn.interactive = true;
    nextBtn.buttonMode = true;

    nextBtn.on("pointerup", () => {
        [arrow, text, nextBtn].forEach(obj => removeSprite(obj));
        callBackFunction();
    });
}

export function addInfoButtonToStage() {
    const infoButton = new PIXI.Sprite.from('./assets/info.png');
    infoButton.anchor.set(0.5);
    infoButton.scale.set(scale);
    infoButton.x = tileDimensions * 2;
    infoButton.y = tileDimensions * 2;
    game.stage.addChild(infoButton);

    infoButton.interactive = true;
    infoButton.buttonMode = true;

    infoButton.on("pointerup", () => {
        RuleBlock.allRuleBlocksArr.forEach(ruleBlock => ruleBlock.stop());
        RuleBlockContainer.children.forEach(ruleBlock => ruleBlock.stop());
        Timer.stop();
        Mechanism.await();
        Chain.stop();

        infoButton.interactive = false;
        infoButton.buttonMode = false;

        makeTip(
            gameWidth / 2,
            MutationBlock.y - tileDimensions,
            Math.PI / 2,
            "This is your starting expression",
            () => {
                makeTip(
                    gameWidth / 2,
                    GoalClass.y - tileDimensions * 3,
                    Math.PI / 2,
                    "You should reach this goal expression by sequence of changes",
                    () => {
                        const arrowX = RuleBlock.allRuleBlocksArr[0].x > gameWidth / 2
                            ? gameWidth / 2 : RuleBlock.allRuleBlocksArr[0].x;
                        makeTip(
                            arrowX,
                            ruleBlocksContainerY + tileDimensions * 11,
                            - Math.PI / 2,
                            "Drag these rule blocks and drop them under the mechanism to apply the rule to starting" +
                            " expression",
                            () => {
                                makeTip(
                                    Timer.sprite.x - tileDimensions * 3,
                                    tileDimensions * 2,
                                    0,
                                    "Don't run out of time and hearts!",
                                    () => {
                                        RuleBlock.allRuleBlocksArr.forEach(ruleBlock => ruleBlock.continue());
                                        Timer.continue();
                                        Chain.continue();
                                        Mechanism.continue();
                                        infoButton.interactive = true;
                                        infoButton.buttonMode = true;
                                    }
                                )
                            }
                        )
                    }
                )
            }
        );
    });
}